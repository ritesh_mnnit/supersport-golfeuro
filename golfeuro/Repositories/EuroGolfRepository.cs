﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace golfeuro.Repositories
{
    public class EuroGolfRepository : IEuroGolfRepository
    {

        public ArrayList GetLatestTournamentIds()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["sql"]);
            ArrayList tournaments = new ArrayList();

            string newQuery = @"SELECT TOP 1 [linkId] as Id FROM [SSZGeneral].[dbo].[golfschedules] 
                                where season = 
                                (
                                    SELECT TOP 1 [Id] FROM [SSZGeneral].[dbo].[golfseasons] 
                                    where Tour = 2 and year(getdate()) = year(EndDate) 
                                    and active = 1 
                                    order by EndDate DESC
                                ) 
                                AND dateadd(day, -1, StartDate) < getDate() 
								AND [linkid] is not null 
								AND [linkid] <> 'NULL'
								order by StartDate DESC";

            using (conn)
            {
                try
                {
                    conn.Open();
                    using (SqlCommand sqlQuery = new SqlCommand(newQuery, conn))
                    {
                        using (SqlDataReader rdr = sqlQuery.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                string id = rdr["Id"].ToString();
                                tournaments.Add(Convert.ToInt32(rdr["Id"]));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return tournaments;
        }
    }
}
