﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace golfeuro.Repositories
{
    public interface IEuroGolfRepository
    {
        ArrayList GetLatestTournamentIds();
    }
}
