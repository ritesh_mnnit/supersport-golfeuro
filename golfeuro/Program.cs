﻿using golfeuro.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace golfeuro
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            if (Environment.UserInteractive)
            {
                Service1 EuroGolfIngestService = new Service1();
                EuroGolfIngestService.EuroGolfRepository = new EuroGolfRepository();
                EuroGolfIngestService.TestStartupAndStop(null);
            }
            else
            {
                Service1 EuroGolfIngestService = new Service1();
                EuroGolfIngestService.EuroGolfRepository = new EuroGolfRepository();

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    EuroGolfIngestService
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
