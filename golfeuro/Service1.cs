﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Threading;
using System.Xml;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using log4net.Config;
using log4net;

namespace golfeuro
{
    public partial class Service1 : ServiceBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Service1));
        public System.Timers.Timer sysTimer;
        public string dbConnString = ConfigurationManager.AppSettings["sql"];
        public Repositories.IEuroGolfRepository EuroGolfRepository 
        { 
            get 
            {
                return _euroGolfRepositry;
            }
            set
            {
                _euroGolfRepositry = value;
            }
        }
        private Repositories.IEuroGolfRepository _euroGolfRepositry;
        public SqlConnection dbConn;
        bool dbConnected = true;
        bool useProxy = Convert.ToBoolean(ConfigurationManager.AppSettings["useProxy"]);
        public string tour = ConfigurationManager.AppSettings["tour"];
        runInfo curRun;

        public Service1()
        {
            InitializeComponent();
        }

        internal void TestStartupAndStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnStop();
        }

        protected override void OnStart(string[] args)
        {
            XmlConfigurator.Configure();                                                   //Necessary to initialize log4net configuration in app.config            
            LogToGrayLog("Application Starting");
            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        protected override void OnStop()
        {
            LogToGrayLog("Application Stopped");
        }

        private void dbConnChanged(object sender, EventArgs e)
        {
            if (dbConnected && dbConn != null)
            {
                if (dbConn.State == ConnectionState.Broken || dbConn.State == ConnectionState.Closed)
                {
                    try
                    {
                        dbConn = new SqlConnection(dbConnString);
                        dbConn.Open();
                    }
                    catch (Exception ex)
                    {
                        dbConnected = false;
                    }
                }
            }
        }

        private void LogToGrayLog(string message)
        {
            log.Info("Euro Golf Ingest App: " + message);
        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(run);
            t.IsBackground = true;
            t.Start();
        }

        public void run()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["sql"]);

            try
            {
                curRun = new runInfo();
                curRun.Start = DateTime.Now;
                curRun.Errors = new List<string>();

                conn.Open();

                ArrayList tournaments = EuroGolfRepository.GetLatestTournamentIds();

                if (tournaments != null && tournaments.Count > 0)
                {
                    foreach (int tournamentId in tournaments)
                    {
                        string leaderboardXml = getLeaderboard(tournamentId);
                        XmlDocument xmlDoc = null;
                        if (!String.IsNullOrEmpty(leaderboardXml))
                        {
                            xmlDoc = new XmlDocument();
                            try
                            {
                                xmlDoc.LoadXml(leaderboardXml);
                            }
                            catch (Exception ex)
                            {
                                LogToGrayLog("Error loading XML Document from Feed Leaderboard - " + ex.Message);
                                xmlDoc = null;
                            }
                        }

                        if (xmlDoc != null)
                        {
                            Leaderboard(xmlDoc, conn);
                        }
                    }
                }

                curRun.End = DateTime.Now;
                endRun();
            }
            catch(Exception exception)
            {
                LogToGrayLog(exception.Message);
            }
            finally
            {
                conn.Dispose();
            }

            sysTimer = new System.Timers.Timer(300000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        private string getLeaderboard(int tournamentId)
        {
            string xml = string.Empty;

            try
            {
                //Old Implementation
                //euro.WSXMLFeed Euro = new euro.WSXMLFeed();
                //if (useProxy)
                //{
                //    Euro.Proxy = returnProxy();
                //}
                //Euro.Timeout = 3600000;
                //System.Net.ServicePointManager.Expect100Continue = false;
                //xml = Euro.GetXMLFeed("SPT@2012", "5c370f54b8ebc425ccbe41102bd74ebd", tournamentId);

                string providerEndpoint = ConfigurationManager.AppSettings["providerEndpointLeaderboard"];

                if (!string.IsNullOrEmpty(providerEndpoint))
                {
                    WebRequest myRequest = WebRequest.Create(providerEndpoint);
                    myRequest.Timeout = 3600000;
                    WebResponse myResponse = myRequest.GetResponse();
                    StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                    xml = objStreamReader.ReadToEnd();

                    FileStream Fs = new FileStream("c:/supersport/golf/euro/xml.txt", FileMode.Create, FileAccess.Write);
                    StreamWriter Sw = new StreamWriter(Fs);
                    Sw.BaseStream.Seek(0, SeekOrigin.Begin);
                    Sw.WriteLine(xml);
                    Sw.Close();
                    Fs.Dispose(); 
                }
                else
                {
                    LogToGrayLog("Failed to load XML Feed - Provider URL Not Specified in Configuration");
                    curRun.Errors.Add("Provider URL Not Specified in Configuration - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                    xml = string.Empty;
                }
            }
            catch (Exception ex)
            {
                LogToGrayLog("Failed to load XML Feed - " + ex.TargetSite.Name + " - " + ex.Message);
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                xml = string.Empty;
            }
            return xml;
        }

        private void Leaderboard(XmlDocument xmlDoc, SqlConnection dbConn)
        {
            SqlCommand sqlQuery;
            StringBuilder sb;

            try
            {
                XmlNodeList xmlEvents = xmlDoc.GetElementsByTagName("event");
                string tournId = string.Empty;
                DateTime updated = new DateTime();
                string tournament = string.Empty;
                foreach (XmlNode EventNode in xmlEvents)
                {
                    string location = string.Empty;
                    string country = string.Empty;
                    int round = 0;
                    int lastRound = 0;
                    string status = string.Empty;
                    DateTime startDate = DateTime.Now;
                    DateTime endDate = DateTime.Now;
                    bool process = true;

                    foreach (XmlAttribute attribute in EventNode.Attributes)
                    {
                        switch (attribute.Name)
                        {
                            case "updatetimestamp":
                                int Year;
                                int Month;
                                int Day;
                                int Hour;
                                int Minute;
                                string[] date1;
                                string[] date2;
                                string tmpDate = attribute.InnerText;

                                if (tmpDate.IndexOf("T") >= 0)
                                {
                                    date1 = tmpDate.Split('T');
                                }
                                else
                                {
                                    date1 = tmpDate.Split(' ');
                                }

                                if (date1[0].IndexOf('-') >= 0)
                                {
                                    date2 = date1[0].Split('-');
                                    Year = Convert.ToInt32(date2[0]);
                                    Month = Convert.ToInt32(date2[1]);
                                    Day = Convert.ToInt32(date2[2]);
                                }
                                else
                                {
                                    //Provider Swopped this
                                    date2 = date1[0].Split('/');
                                    Year = Convert.ToInt32(date2[2]);
                                    Month = Convert.ToInt32(date2[1]);
                                    Day = Convert.ToInt32(date2[0]);
                                }

                                date1 = date1[1].Split(':');
                                Hour = Convert.ToInt32(date1[0]);
                                Minute = Convert.ToInt32(date1[1]);
                                updated = new DateTime(Year, Month, Day, Hour, Minute, 0);
                                break;
                            default:
                                break;
                        }
                    }

                    XmlNodeList xmlTournaments = EventNode.SelectNodes("tournament");
                    foreach (XmlNode TournamentNode in xmlTournaments)
                    {
                        tournId = TournamentNode.Attributes["tournid"].InnerText;

                        if (String.IsNullOrEmpty(tournId))
                        {
                            process = false;
                        }

                        if (process)
                        {
                            foreach (XmlAttribute attribute in TournamentNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "tournid":
                                        tournId = attribute.InnerText.ToString();
                                        break;
                                    case "name":
                                        tournament = attribute.InnerText.ToString();
                                        break;
                                    case "location":
                                        location = attribute.InnerText.ToString();
                                        break;
                                    case "country":
                                        country = attribute.InnerText.ToString();
                                        break;
                                    case "lastroundcompleted":
                                        lastRound = string.IsNullOrEmpty(attribute.InnerText) ? 0 : Convert.ToInt32(attribute.InnerText);
                                        break;
                                    case "currentround":
                                        round = Convert.ToInt32(attribute.InnerText.ToString());
                                        break;
                                    case "status":
                                        status = attribute.InnerText.ToString();
                                        status = FixStatus(status);
                                        break;
                                    case "begindate":
                                        if (!String.IsNullOrEmpty(attribute.InnerText) && attribute.InnerText.IndexOf("/") >= 0)
                                        {
                                            string[] tmpStartDate = attribute.InnerText.ToString().Split('/');
                                            try
                                            {
                                                startDate = new DateTime(2000 + Convert.ToInt32(tmpStartDate[2]), Convert.ToInt32(tmpStartDate[1]), Convert.ToInt32(tmpStartDate[0]));
                                            }
                                            catch (Exception ex1)
                                            {
                                                startDate = DateTime.Now;
                                            }
                                        }
                                        break;
                                    case "enddate":
                                        if (!String.IsNullOrEmpty(attribute.InnerText) && attribute.InnerText.IndexOf("/") >= 0)
                                        {
                                            string[] tmpEndDate = attribute.InnerText.ToString().Split('/');
                                            try
                                            {
                                                endDate = new DateTime(2000 + Convert.ToInt32(tmpEndDate[2]), Convert.ToInt32(tmpEndDate[1]), Convert.ToInt32(tmpEndDate[0]));
                                            }
                                            catch (Exception ex1)
                                            {
                                                endDate = DateTime.Now;
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            XmlNodeList xmlCourses = TournamentNode.SelectNodes("courses/course");
                            foreach (XmlNode courseNode in xmlCourses)
                            {
                                string course = string.Empty;
                                int metres = 0;
                                int yards = 0;
                                int frontyards = 0;
                                int backyards = 0;
                                int par = 0;
                                int frontpar = 0;
                                int backpar = 0;
                                int holes = 0;

                                foreach (XmlAttribute attribute in courseNode.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "totalyards":
                                            yards = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "backyards":
                                            backyards = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "frontyards":
                                            frontyards = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "totalmetres":
                                            metres = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "par":
                                            par = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "backpar":
                                            backpar = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "frontpar":
                                            frontpar = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "holes":
                                            holes = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "no":
                                            course = attribute.InnerText.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                sb = new StringBuilder();
                                sb.Append("DELETE FROM golf.dbo.leaderboardCourseHoles WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + " AND course = '" + course.Replace("'", "''") + "');");
                                XmlNodeList xmlHoles = courseNode.SelectNodes("hole");
                                foreach (XmlNode holeNode in xmlHoles)
                                {
                                    int holeNumber = 0;
                                    int holePar = 0;
                                    int holeYards = 0;
                                    int holeMetres = 0;

                                    foreach (XmlAttribute attribute in holeNode.Attributes)
                                    {
                                        switch (attribute.Name)
                                        {
                                            case "no":
                                                holeNumber = Convert.ToInt32(attribute.InnerText.ToString());
                                                break;
                                            case "par":
                                                holePar = Convert.ToInt32(attribute.InnerText.ToString());
                                                break;
                                            case "yards":
                                                holeYards = Convert.ToInt32(attribute.InnerText.ToString());
                                                break;
                                            case "metres":
                                                holeMetres = Convert.ToInt32(attribute.InnerText.ToString());
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    sb.Append("INSERT INTO golf.dbo.leaderboardCourseHoles (tour, tournament, course, year, hole, par, metres, yards,lastUpdated) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', '" + course.Replace("'", "''") + "', " + DateTime.Now.Year + ", " + holeNumber + ", " + holePar + ", " + holeMetres + ", " + holeYards + ", Getdate());");
                                }

                                sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboardCourse WHERE (tour = @tour AND tournament = @tournament AND year = @year AND course = @course)", dbConn);
                                sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                                sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                                sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                                sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                                int coursePresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                                if (coursePresent <= 0)
                                {
                                    sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboardCourse (tour, tournament, course, year, lengthYards, lengthMetres, frontNineLengthYards, backNineLengthYards, par, frontNinePar, backNinePar, holes, lastUpdated) VALUES (@tour, @tournament, @course, @year, @lengthYards, @lengthMetres, @frontNineLengthYards, @backNineLengthYards, @par, @frontNinePar, @backNinePar, @holes, @lastUpdated)", dbConn);
                                    sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                                    sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                                    sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                                    sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                                    sqlQuery.Parameters.Add("@lengthYards", SqlDbType.Int).Value = yards;
                                    sqlQuery.Parameters.Add("@lengthMetres", SqlDbType.Int).Value = metres;
                                    sqlQuery.Parameters.Add("@frontNineLengthYards", SqlDbType.Int).Value = frontyards;
                                    sqlQuery.Parameters.Add("@backNineLengthYards", SqlDbType.Int).Value = backyards;
                                    sqlQuery.Parameters.Add("@par", SqlDbType.Int).Value = par;
                                    sqlQuery.Parameters.Add("@frontNinePar", SqlDbType.Int).Value = frontpar;
                                    sqlQuery.Parameters.Add("@backNinePar", SqlDbType.Int).Value = backpar;
                                    sqlQuery.Parameters.Add("@holes", SqlDbType.Int).Value = holes;
                                    sqlQuery.Parameters.Add("@lastUpdated", SqlDbType.DateTime).Value = DateTime.Now;
                                    sqlQuery.ExecuteNonQuery();

                                    if (!String.IsNullOrEmpty(sb.ToString()))
                                    {
                                        sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                                        sqlQuery.ExecuteNonQuery();
                                    }
                                }
                            }

                            XmlNodeList xmlPlayers = TournamentNode.SelectNodes("players/player");
                            sb = new StringBuilder();
                            StringBuilder sb1 = new StringBuilder();
                            StringBuilder sb2 = new StringBuilder();
                            if (xmlPlayers.Count > 0)
                            {
                                sb.Append("DELETE FROM golf.dbo.leaderboardScores WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");");
                                sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardPlayerRounds WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");", dbConn);
                                sqlQuery.ExecuteNonQuery();
                                sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardPlayerRoundHoles WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");", dbConn);
                                sqlQuery.ExecuteNonQuery();
                            }
                            int sorting = 0;
                            foreach (XmlNode PlayerNode in xmlPlayers)
                            {
                                string id = string.Empty;
                                string firstname = string.Empty;
                                string lastname = string.Empty;
                                string playerCountry = string.Empty;
                                int holesPlayed = 0;
                                string round1ToPar = string.Empty;
                                string round1Strokes = string.Empty;
                                string round2ToPar = string.Empty;
                                string round2Strokes = string.Empty;
                                string round3ToPar = string.Empty;
                                string round3Strokes = string.Empty;
                                string round4ToPar = string.Empty;
                                string round4Strokes = string.Empty;
                                int pos = 1000;
                                int tied = 0;
                                string total = string.Empty;
                                string playerStatus = string.Empty;
                                bool insert = true;

                                sb2 = new StringBuilder();

                                string pattern = @"[^a-zA-Z\s]";

                                foreach (XmlAttribute attribute in PlayerNode.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "country":
                                            playerCountry = attribute.InnerText.ToString();
                                            break;
                                        case "firstname":
                                            firstname = attribute.InnerText.ToString();
                                            break;
                                        case "lastname":
                                            lastname = attribute.InnerText.ToString();
                                            List<string> lastnameList = lastname.Select(c => c.ToString()).ToList();
                                            for (int i = 1; i < lastnameList.Count; i++)
                                            {
                                                if (System.Text.RegularExpressions.Regex.IsMatch(lastnameList[i], pattern))
                                                    lastnameList[i] = lastnameList[i].ToLower();
                                            }

                                            StringBuilder newlastname = new StringBuilder();
                                            foreach (string letter in lastnameList)
                                                newlastname.Append(letter);

                                            lastname = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo
                                                .ToTitleCase(newlastname.ToString().ToLower());
                                            //lastname = newlastname.ToString();

                                            break;
                                        case "id":
                                            id = attribute.InnerText.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                XmlNode TotalsNode = PlayerNode.SelectSingleNode("totals");
                                foreach (XmlAttribute attribute in TotalsNode.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "position":
                                            try
                                            {
                                                pos = Convert.ToInt32(attribute.InnerText.ToString()) == 0 ? 1000 : Convert.ToInt32(attribute.InnerText.ToString());
                                            }
                                            catch
                                            {
                                                pos = 1000;
                                            }
                                            break;
                                        case "status":
                                            playerStatus = attribute.InnerText.ToString();
                                            break;
                                        case "tied":
                                            tied = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "totaltopar":
                                            total = attribute.InnerText.ToString();
                                            if (total.IndexOf("-") < 0 && total.IndexOf("0") < 0 && total.IndexOf("E") < 0)
                                            {
                                                total = "+" + total;
                                            }
                                            else if (round1ToPar == "0")
                                            {
                                                total = "E";
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                ArrayList statusses = new ArrayList();
                                statusses.Add("ok");
                                statusses.Add("unofficial");
                                statusses.Add("fpt");
                                statusses.Add("pn");

                                if (!(statusses.Contains(playerStatus.ToLower())))
                                {
                                    insert = false;
                                }

                                XmlNodeList xmlRounds = PlayerNode.SelectNodes("round");
                                foreach (XmlNode Round in xmlRounds)
                                {
                                    int roundNumber = 0;
                                    if (Round.Attributes["no"] != null)
                                    {
                                        try
                                        {
                                            roundNumber = Convert.ToInt32(Round.Attributes["no"].InnerText);
                                        }
                                        catch
                                        {

                                        }
                                    }

                                    int tmpHolesPlayed = 0;
                                    string tmpStrokes = string.Empty;
                                    string tmpPar = string.Empty;

                                    string roundToPar = string.Empty;
                                    int roundstrokes = 0;
                                    DateTime roundTeeTime = new DateTime(1900, 1, 1, 0, 0, 0);
                                    int roundStartingTee = 1;
                                    int roundMatchNumberIndex = 0;
                                    int roundMatchNumber = 0;
                                    int roundHolePlayed = 0;
                                    string roundDescription = string.Empty;
                                    string roundCourse = string.Empty;

                                    foreach (XmlAttribute attribute in Round.Attributes)
                                    {
                                        switch (attribute.Name)
                                        {
                                            case "holesplayed":
                                                try
                                                {
                                                    tmpHolesPlayed = Convert.ToInt32(attribute.InnerText.ToString());
                                                    roundHolePlayed = tmpHolesPlayed;
                                                }
                                                catch
                                                { }
                                                break;
                                            case "strokes":
                                                tmpStrokes = attribute.InnerText.ToString();
                                                try
                                                {
                                                    roundstrokes = Convert.ToInt32(attribute.InnerText.ToString());
                                                }
                                                catch
                                                { }
                                                break;
                                            case "totaltopar":
                                                tmpPar = attribute.InnerText.ToString();
                                                roundToPar = tmpPar;
                                                break;
                                            case "startingtee":
                                                try
                                                {
                                                    roundStartingTee = Convert.ToInt32(attribute.InnerText.ToString());
                                                }
                                                catch
                                                { }
                                                break;
                                            case "matchnumber":
                                                try
                                                {
                                                    roundMatchNumber = Convert.ToInt32(attribute.InnerText.ToString());
                                                }
                                                catch
                                                { }
                                                break;
                                            case "matchnumberindex":
                                                try
                                                {
                                                    roundMatchNumberIndex = Convert.ToInt32(attribute.InnerText.ToString());
                                                }
                                                catch
                                                { }
                                                break;
                                            case "description":
                                                roundDescription = attribute.InnerText.ToString();
                                                break;
                                            case "course":
                                                roundCourse = attribute.InnerText.ToString();
                                                break;
                                            case "teetime":
                                                try
                                                {
                                                    roundTeeTime = Convert.ToDateTime("1900-01-01 " + attribute.InnerText.ToString());
                                                }
                                                catch
                                                { }
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    if (round == roundNumber)
                                    {
                                        holesPlayed = tmpHolesPlayed;
                                    }

                                    switch (roundNumber)
                                    {
                                        case 1:
                                            round1ToPar = tmpPar;
                                            if (round1ToPar.IndexOf("-") < 0 && round1ToPar.IndexOf("0") < 0 && round1ToPar.IndexOf("E") < 0)
                                            {
                                                round1ToPar = "+" + round1ToPar;
                                            }
                                            else if (round1ToPar == "0")
                                            {
                                                round1ToPar = "E";
                                            }
                                            if (tmpHolesPlayed == 18)
                                            {
                                                round1Strokes = tmpStrokes;
                                            }
                                            break;
                                        case 2:
                                            round2ToPar = tmpPar;
                                            if (round2ToPar.IndexOf("-") < 0 && round2ToPar.IndexOf("0") < 0 && round2ToPar.IndexOf("E") < 0)
                                            {
                                                round2ToPar = "+" + round2ToPar;
                                            }
                                            else if (round1ToPar == "0")
                                            {
                                                round2ToPar = "E";
                                            }
                                            if (tmpHolesPlayed == 18)
                                            {
                                                round2Strokes = tmpStrokes;
                                            }
                                            break;
                                        case 3:
                                            round3ToPar = tmpPar;
                                            if (round3ToPar.IndexOf("-") < 0 && round3ToPar.IndexOf("0") < 0 && round3ToPar.IndexOf("E") < 0)
                                            {
                                                round3ToPar = "+" + round3ToPar;
                                            }
                                            else if (round1ToPar == "0")
                                            {
                                                round3ToPar = "E";
                                            }
                                            if (tmpHolesPlayed == 18)
                                            {
                                                round3Strokes = tmpStrokes;
                                            }
                                            break;
                                        case 4:
                                            round4ToPar = tmpPar;
                                            if (round4ToPar.IndexOf("-") < 0 && round4ToPar.IndexOf("0") < 0 && round4ToPar.IndexOf("E") < 0)
                                            {
                                                round4ToPar = "+" + round4ToPar;
                                            }
                                            else if (round1ToPar == "0")
                                            {
                                                round4ToPar = "E";
                                            }
                                            if (tmpHolesPlayed == 18)
                                            {
                                                round4Strokes = tmpStrokes;
                                            }
                                            break;
                                        default:
                                            break;
                                    }

                                    XmlNodeList xmlHoles = Round.SelectNodes("score");
                                    foreach (XmlNode Hole in xmlHoles)
                                    {
                                        int holeNumber = 0;
                                        int holeStrokes = 0;
                                        int holePutts = 0;
                                        int holeGir = 0;
                                        int holeFairways = 0;
                                        int holeDrive = 0;
                                        int holeBunkers = 0;
                                        foreach (XmlAttribute attribute in Hole.Attributes)
                                        {
                                            switch (attribute.Name)
                                            {
                                                case "hole":
                                                    holeNumber = Convert.ToInt32(attribute.InnerText);
                                                    break;
                                                case "strokes":
                                                    holeStrokes = Convert.ToInt32(attribute.InnerText);
                                                    break;
                                                case "putts":
                                                    holePutts = Convert.ToInt32(attribute.InnerText);
                                                    break;
                                                case "gir":
                                                    holeGir = Convert.ToInt32(attribute.InnerText);
                                                    break;
                                                case "fairway":
                                                    holeFairways = Convert.ToInt32(attribute.InnerText);
                                                    break;
                                                case "drive":
                                                    holeDrive = Convert.ToInt32(attribute.InnerText);
                                                    break;
                                                case "bunkers":
                                                    holeBunkers = Convert.ToInt32(attribute.InnerText);
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        if (insert)
                                        {
                                            sb2.Append("INSERT INTO golf.dbo.leaderboardPlayerRoundHoles (tour, tournament, year, player, round, hole, strokes, putts, gir, fairways, drive, bunkers, lastUpdated) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", '" + id + "', " + roundNumber + ", " + holeNumber + ", " + holeStrokes + ", " + holePutts + ", " + holeGir + ", " + holeFairways + ", " + holeDrive + ", " + holeBunkers + ", GetDate());");
                                        }
                                    }
                                    if (insert)
                                    {
                                        sb1.Append("INSERT INTO golf.dbo.leaderboardPlayerRounds (tour, tournament, year, course, player, roundNumber, par, strokes, teeTime, startingTee, matchNumberIndex, matchNumber, holesPlayed, description, lastUpdated) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", '" + roundCourse.Replace("'", "''") + "', '" + id + "', " + roundNumber + ", '" + roundToPar + "', " + roundstrokes + ", '" + roundTeeTime.ToString("yyyy-MM-dd HH:mm") + "', " + roundStartingTee + ", " + roundMatchNumberIndex + ", " + roundMatchNumber + ", " + roundHolePlayed + ", '" + roundDescription + "', GetDate());");
                                    }
                                }

                                if (!String.IsNullOrEmpty(sb2.ToString()))
                                {
                                    sqlQuery = new SqlCommand(sb2.ToString(), dbConn);
                                    sqlQuery.ExecuteNonQuery();
                                }

                                if (insert)
                                {
                                    sb.Append("INSERT INTO golf.dbo.leaderboardScores (tour, tournament, year, position, sorting, tied, playerId, firstName, lastName, country, round1topar, round2topar, round3topar, round4topar, round1strokes, round2strokes, round3strokes, round4strokes, total, holesPlayed) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", " + pos + ", " + sorting + ", " + tied + ", " + id + ", '" + firstname.Replace("'", "''") + "', '" + lastname.Replace("'", "''") + "', '" + playerCountry.Replace("'", "''") + "', '" + round1ToPar + "', '" + round2ToPar + "', '" + round3ToPar + "', '" + round4ToPar + "', '" + round1Strokes + "', '" + round2Strokes + "', '" + round3Strokes + "', '" + round4Strokes + "', '" + total + "', " + holesPlayed + ");");
                                    //sorting += 1;
                                }
                            }

                            if (!String.IsNullOrEmpty(tournId))
                            {
                                if (!String.IsNullOrEmpty(sb1.ToString()))
                                {
                                    sqlQuery = new SqlCommand(sb1.ToString(), dbConn);
                                    sqlQuery.ExecuteNonQuery();
                                }

                                if (!String.IsNullOrEmpty(sb.ToString()))
                                {
                                    sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                                    sqlQuery.ExecuteNonQuery();
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(tournId))
                        {
                            sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboard WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                            sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                            int leaderboardPresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                            if (leaderboardPresent > 0)
                            {
                                sqlQuery = new SqlCommand("UPDATE golf.dbo.leaderboard SET name = @name, location = @location, country = @country, lastRound = @lastRound, round = @round, status = @status, startDate = @startDate, endDate = @endDate, updateDate = @updateDate, timeStamp = @timeStamp WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                            }
                            else
                            {
                                sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboard (tour, tournament, year, type, name, shortname, location, country, lastRound, round, status, startDate, endDate, updateDate, timeStamp) VALUES (@tour, @tournament, @year, @type, @name, @shortname, @location, @country, @lastRound, @round, @status, @startDate, @endDate, @updateDate, @timeStamp)", dbConn);
                            }

                            if (round < 1)
                            {
                                round = 1;
                            }
                            sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                            sqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = "";
                            sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = tournament;
                            sqlQuery.Parameters.Add("@shortname", SqlDbType.VarChar).Value = tournament;
                            sqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                            sqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
                            sqlQuery.Parameters.Add("@lastRound", SqlDbType.Int).Value = lastRound;
                            sqlQuery.Parameters.Add("@round", SqlDbType.Int).Value = round;
                            sqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                            sqlQuery.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
                            sqlQuery.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;
                            sqlQuery.Parameters.Add("@updateDate", SqlDbType.DateTime).Value = DateTime.Now;
                            sqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = updated;
                            sqlQuery.ExecuteNonQuery();
                        }
                    }
                }
                Console.WriteLine(DateTime.Now.ToString("u") + " : Data Ingested for Tournament " + tournament + " Id " + tournId + " UpdateTimeStamp " + updated.ToString());
            }
            catch (Exception ex)
            {
                LogToGrayLog("Error updating leaderboard to Database from XML - " + ex.Message);
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
            }
        }

        private WebProxy returnProxy()
        {
            WebProxy tmpProxy = new WebProxy();
            FileStream Fs = new FileStream("C:\\SuperSport\\proxyDetails.txt", FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader Sw = new StreamReader(Fs);
            Sw.BaseStream.Seek(0, SeekOrigin.Begin);
            string proxyUsername = Sw.ReadLine();
            string proxyPassword = Sw.ReadLine();
            string proxyDomain = Sw.ReadLine();
            string proxyAddress = Sw.ReadLine();
            int proxyPort = Convert.ToInt32(Sw.ReadLine());
            Sw.Close();
            Fs.Close();
            Fs.Dispose();

            System.Net.NetworkCredential ProxyCredentials = new System.Net.NetworkCredential(proxyUsername, proxyPassword, proxyDomain);
            tmpProxy = new System.Net.WebProxy(proxyAddress, proxyPort);
            tmpProxy.Credentials = ProxyCredentials;

            return tmpProxy;
        }

        //private void writeInfo()
        //{
        //    try
        //    {
        //        FileStream Fs = new FileStream(ConfigurationManager.AppSettings["log"], FileMode.Create, FileAccess.Write);
        //        StreamWriter Sw = new StreamWriter(Fs);
        //        Sw.BaseStream.Seek(0, SeekOrigin.Begin);
        //        Sw.WriteLine("Start: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
        //        Sw.WriteLine("End: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
        //        foreach (string tmpString in curRun.Errors)
        //        {
        //            Sw.WriteLine("Error: " + tmpString);
        //        }
        //        Sw.Close();
        //        Fs.Dispose();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}

        private void endRun()
        {
            try
            {
                string errors = "";

                foreach (string tmpString in curRun.Errors)
                {
                    errors += "Error: " + tmpString + Environment.NewLine;
                }

                using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.AppSettings["sql"]))
                {
                    dbConn.Open();
                    string queryText = @"Insert Into golf.dbo.leaderboardIngest 
                                        (tournament, startTime, endTime, errors, timeStamp) 
                                        Values (2, @startTime, @endTime, @errors, @timeStamp)";
                    
                    using (SqlCommand SqlQuery = new SqlCommand(queryText, dbConn))
                    {
                        SqlQuery.Parameters.Add("@startTime", SqlDbType.DateTime).Value = curRun.Start;
                        SqlQuery.Parameters.Add("@endTime", SqlDbType.DateTime).Value = curRun.End;
                        SqlQuery.Parameters.Add("@errors", SqlDbType.VarChar).Value = errors;
                        SqlQuery.Parameters.Add("@timeStamp", SqlDbType.VarChar).Value = DateTime.Now;
                        SqlQuery.ExecuteNonQuery();
                    }

                    dbConn.Close();
                }
            }
            catch (Exception ex)
            {
                LogToGrayLog("Error updating run details to Database - " + ex.Message);
                curRun.Errors.Add("Updating run details - " + ex.Message);
            }
        }

        private string FixStatus(string status)
        {
            string configOptions = ConfigurationManager.AppSettings["statuses"];
            string tempStatus = status.ToLower();

            Hashtable statuses = new Hashtable();
            string[] options = configOptions.Split(';');
            foreach (string option in options)
            {
                string[] values = option.Split('|');
                statuses.Add(values[0].ToString(), values[1].ToString());
            }

            if (statuses.ContainsKey(tempStatus))
            {
                status = statuses[tempStatus].ToString();
            }

            return status;
        }
    }
}